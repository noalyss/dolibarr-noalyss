# Import Dolibarr

## Description

This plugin for noalyss let you import files from Dolibar in NOALYSS. It must be installed on Noalyss. 

Noalyss is a very professional accountancy web software in opensource (0+ERP) , compatible with France, Belgium and OHADA rules. It is a "accountancy server" made to be used by hundred of users for hundred of companies hosted in one server. It is the very first in this category

Copy this folder in noalyss/include/ext and activate the plugin in "Parameter" -> Plugin


More information in french https://www.noalyss.eu


