# INTERFACE DOLIBARR vers NOALYSS (https://www.dolibarr.org) (https://www.noalyss.eu/)

## Features
Ce modules permet de créer les fichiers CSV, qui, par la suite permettrons de créer les fiches de Noyss.
Il permet aussi d'exporter les factures Dolibarr au format CSV de Noalyss, pour ensuite les importer dans Noalyss.
Avant d'importer les factures dans Noalyss, il faut, bien entendu, que toutes les fiches (clients, produits, services, ....) soient déjà importées dans Noalyss.
                                                                                                                                            
Attention chaque type de fiche : produit, service, client, fournisseur et facture doivent avoir une référence. cette référence est utilisée pour donner le nom de la fiche dans Noalyss.

Tous les fichiers créés seront déposés dans le répertoire noalyss (ou celui defini dans le setup de ce module)  de la gestion des documents (Point de menu 'Documents' de la barre d'outil supérieur). Donc n'oubliez pas de ne pas désactiver ce module GED avant l'utilisation de celui-ci, sinon vous ne retrouverez pas vos fichiers d'export.

la documentation se trouve ici : http://wiki.noalyss.eu/doku.php?id=importation_dolibarr


## Translations

Les fichiers de langues peuvent êtres définis manuellement par l'édition/ajout des fichiers dans le sous-répertoire  *langs* de ce module.


## Installation

### From the ZIP file and GUI interface

Aller dans le menu ```Accueil- Configuration - Modules/Applications - Déployer/Installer un module externe ``` Selectionnez le fichier ZIP du module et cliquez sur le bouton SEND

Note: Si vous avez un message qui vous dit "Vous n'avez pas de répertoire 'custom', vérifiez votre fichier de configuration 

- Dans votre répertoire d'installation de Dolibarr, editez le fichier ```htdocs/conf/conf.php``` et vérifiez si les lignes suivantes ne sont pas commentées:

    ```php
    //$dolibarr_main_url_root_alt ...
    //$dolibarr_main_document_root_alt ...
    ```

- Décommentez si necessaire (effacez les ```//``` devant les 2 lignes) at ajoutez la bonne information

    Par exemple :

    - UNIX:
        ```php
        $dolibarr_main_url_root_alt = '/custom';
        $dolibarr_main_document_root_alt = '/var/www/Dolibarr/htdocs/custom';
        ```

    - Windows:
        ```php
        $dolibarr_main_url_root_alt = '/custom';
        $dolibarr_main_document_root_alt = 'C:/My Web Sites/Dolibarr/htdocs/custom';
        ```
        
### From a GIT repository

- Clone the repository in ```$dolibarr_main_document_root_alt/noalyss```

```sh
cd ....../custom
git clone git@github.com:gitlogin/dolibarr-noalyss.git noalyss
```

### <a name="final_steps"></a>Final steps

From your browser:

  - Log into Dolibarr as a super-administrator
  - Go to "Setup" -> "Modules"
  - You should now be able to find and enable the module


## Licenses

**Main code**

GPLv3 or (at your option) any later version. See file COPYING for more information.

**Documentation**

All texts and readmes are licensed under GFDL.
