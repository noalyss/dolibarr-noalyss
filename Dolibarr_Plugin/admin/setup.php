<?php
/* Copyright (C) 2012 ALLTUX  <info@alltux.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**
 *    \file       htdocs/phpcompta/admin/config.php
 *    \ingroup    phpcompta
 *    \brief      Page de configuration du module phpcompta
 *    \version    $Id: config.php,v 0.1 2011/04015 14:27:47 grandoc Exp $
 */

$res = @include("../../main.inc.php"); // From htdocs directory
if (! $res) {
    $res = @include("../../../main.inc.php"); // From "custom" directory
}
require_once DOL_DOCUMENT_ROOT.'/core/lib/admin.lib.php';

if (!$user->admin)
    accessforbidden();

$langs->load("admin");
$langs->load("other");
$langs->load("noalyss@noalyss");

$def = array();

$ok=True;
if (!$conf->global->NOALYSS_DIR) {
    $ok=false;
    $_POST["NOALYSS_DIR"]="noalyss";
}
// Sauvegardes parametres
if ($_POST["action"] == 'update' || !$ok)
{
    $i=0;
    $db->begin();
    $i+=dolibarr_set_const($db,'NOALYSS_DIR',trim($_POST["NOALYSS_DIR"]),'chaine',0,'',$conf->entity);

    if ($i >= 1)
    {
        $db->commit();
        $mesg = "<font class=\"ok\">".$langs->trans("SetupSaved")."</font>";
        $dir=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR;
        if (!file_exists($dir)) {
            if (mkdir($dir)) {
                $sql="insert into ".MAIN_DB_PREFIX."ecm_directories (label, entity, fk_parent, cachenbofdoc,date_c,fk_user_c) values ('".$conf->global->NOALYSS_DIR."',1,0,0,now(),1)";
                $db->query($sql);
            }
        }
    }
    else
    {
        $db->rollback();
        $mesg=$db->lasterror();
    }
}


/**
 * View
 */

llxHeader();

$linkback='<a href="'.DOL_URL_ROOT.'/admin/modules.php">'.$langs->trans("BackToModuleList").'</a>';
print_fiche_titre($langs->trans("NoalyssSetup"),$linkback,'setup');
print '<br>';


print '<form name="noalyssconfig" action="'.$_SERVER["PHP_SELF"].'" method="POST">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="update">';
print "<table class=\"noborder\" width=\"100%\">";

print "<tr class=\"liste_titre\">";
print "<td width=\"30%\">".$langs->trans("Parameter")."</td>";
print "<td>".$langs->trans("Value")."</td>";
print "<td>".$langs->trans("Examples")."</td>";
print "</tr>";

print "<tr class=\"impair\">";
print "<td>".$langs->trans("TitreDirDest")."</td>";
print "<td><input type=\"text\" class=\"flat\" name=\"NOALYSS_DIR\" value=\"". ($_POST["NOALYSS_DIR"]?$_POST["NOALYSS_DIR"]:$conf->global->NOALYSS_DIR) . "\" size=\"40\"></td>";
print "<td>noalyss</td>";
print "</tr>";

print "</table>";

print '<br><center>';
print "<input type=\"submit\" name=\"save\" class=\"button\" value=\"".$langs->trans("Save")."\">";
print "</center>";

print "</form>\n";

if ($mesg) print "<br>$mesg<br>";

$db->close();
llxFooter('$Date: 2013/12/20 14:27:47 $ - $Revision: 0.1 $');
?>
