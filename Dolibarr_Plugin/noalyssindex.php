<?php
/* Copyright (C)2012 ALLTUX  <info@alltux.be>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

/**
 *       \file       htdocs/noalyss/index.php
 *       \ingroup    noalyss
 *       \brief      Home page of export to Noalyss
 *       \version    $Id: index.php,v 0.1 2012/04/16 12:16:43 grandoc Exp $
 */

$res = 0;
if (! $res && file_exists("../main.inc.php")) {
    $res = @include("../main.inc.php");
}
if (! $res && file_exists("../../main.inc.php")) {
    $res = @include("../../main.inc.php");
}
if (! $res && file_exists("../../../main.inc.php")) {
    $res = @include("../../../main.inc.php");
}
// The following should only be used in development environments
if (! $res && file_exists("../../../dolibarr/htdocs/main.inc.php")) {
    $res = @include("../../../dolibarr/htdocs/main.inc.php");
}
if (! $res && file_exists("../../../../dolibarr/htdocs/main.inc.php")) {
    $res = @include("../../../../dolibarr/htdocs/main.inc.php");
}
if (! $res && file_exists("../../../../../dolibarr/htdocs/main.inc.php")) {
    $res = @include("../../../../../dolibarr/htdocs/main.inc.php");
}
if (! $res) {
    die("Main include failed");
}

require_once(DOL_DOCUMENT_ROOT."/exports/class/export.class.php");

function propre($s) {
    $ret=str_replace('"','',$s);
    $ret=str_replace("\r",'',$ret);
    $ret=str_replace("\n",'',$ret);
    $ret=str_replace("<br>",'',$ret);
    $ret=str_replace("<br />",'',$ret);
    return $ret;
}

function updateNbreFichier($db,$dir) {
    $num = 0;
    $dir_handle = opendir($dir);
    while($entry = readdir($dir_handle)) {
        if(is_file($dir.'/'.$entry)) $num++;
    }
    closedir($dir_handle);
    $tmp=explode("/",$dir);
    if (strpos($dir,"\\")) $tmp=explode("\\",$dir);
    $label=$tmp[count($tmp)-1];
    $sql="update ".MAIN_DB_PREFIX."ecm_directories set cachenbofdoc=".$num." where label='".$label."'";
    $res=$db->query($sql);
    $db->free($res);
    return $res;
}

$langs->load("noalyss@noalyss");

if (! $user->societe_id == 0) accessforbidden();

$export=new Export($db);
$export->load_arrays($user);
/*
 * les actions
 */

$msg="";
$erreur="";
$erreurfact="";
$dd=$object->date;
$df=$object->date;

if (is_dir($dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR)==false) mkdir($dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR, 0777, true);

$action = isset($_GET["action"])?$_GET["action"]:$_POST['action'];
if ($action=="ficheclient") {
    $annee = GETPOST("annee",'int');
    $sql ="SELECT s.nom, s.code_client, s.address, s.zip, s.town, s.phone, s.fax, s.email, s.tva_assuj, s.tva_intra, s.code_compta, p.code ";
    $sql.=" FROM ".MAIN_DB_PREFIX."societe s,".MAIN_DB_PREFIX."c_country p where s.fk_pays=p.rowid and (client=1 or client=3) and code_client is not null and code_client<>'' ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql)
    {
        $fn=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/client_".strval($annee)."_".date('Ymd').".csv";
        $out=fopen($fn,'w');
        while ($objp = $db->fetch_object($resql))
        {
            //$row=sprintf('"%s","%s","%s",%s,"%s","%s","%s","%s"', $objp->code_client, propre($objp->nom), propre($objp->address), $objp->zip, $objp->town, $objp->phone, $objp->email, $objp->tva_intra);
            $row=sprintf('"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s"', propre($objp->address), $objp->zip, $objp->email, propre($objp->nom), $objp->tva_intra, $objp->code, "", $objp->code_compta, $objp->code_client, $objp->phone, $objp->town);
            fwrite($out,$row."\r\n");
        }
        fclose($out);
        $db->free($resql);
        updateNbreFichier($db,dirname($fn));
        $msg=$langs->trans("PHPComptaMsgClient", basename($fn));
    } else {
        $msg=$langs->trans("PHPComptaMsgNoFile");
    }
    $sql="select count(*) as nbre from ".MAIN_DB_PREFIX."societe where (client=1 or client=3) and (code_client is null or code_client='') ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql) {
        $objp = $db->fetch_object($resql);
        if ($objp->nbre > 0) $erreur=$langs->trans("PHPComptaMsgNbre",strval($objp->nbre));
    }
}

if ($action=="fichefournisseur") {
    $annee = GETPOST("annee",'int');
    $sql ="SELECT s.nom, s.code_fournisseur, s.address, s.zip, s.town, s.phone, s.fax, s.email, s.tva_assuj, s.tva_intra, s.code_compta_fournisseur, p.code ";
    $sql.=" FROM ".MAIN_DB_PREFIX."societe s,".MAIN_DB_PREFIX."c_country p where s.fk_pays=p.rowid and fournisseur=1  and code_fournisseur is not null and code_fournisseur<>'' ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql)
    {
        $fn=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/fournisseur_".strval($annee)."_".date('Ymd').".csv";
        $out=fopen($fn,'w');
        while ($objp = $db->fetch_object($resql))
        {
            //$row=sprintf('"%s","%s","%s",%s,"%s","%s","%s","%s"', $objp->code_fournisseur, propre($objp->nom), propre($objp->address), $objp->zip, $objp->town, $objp->phone, $objp->email, $objp->tva_intra);
            $row=sprintf('"%s","%s","%s","%s","%s","%s","%s","%s","%s","%s","%s"', propre($objp->address), $objp->zip, $objp->email, propre($objp->nom), $objp->tva_intra, $objp->code, "", $objp->code_compta_fournisseur, $objp->code_fournisseur, $objp->phone, $objp->town);
            fwrite($out,$row."\r\n");
        }
        fclose($out);
        $db->free($resql);
        updateNbreFichier($db,dirname($fn));
        $msg=$langs->trans("PHPComptaMsgFournisseur", basename($fn));
    } else {
        $msg=$langs->trans("PHPComptaMsgNoFile");
    }

    $sql="select count(*) as nbre from ".MAIN_DB_PREFIX."societe where fournisseur=1 and (code_fournisseur is null or code_fournisseur='') ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql) {
        $objp = $db->fetch_object($resql);
        if ($objp->nbre > 0) $erreur=$langs->trans("PHPComptaMsgNbre",strval($objp->nbre));
    }
}

if ($action=="ficheproduit") {
    // champs de sorrtie Noalyss : nom produit, poste comptable, prix achat, quick code, taux TVA
    $annee = GETPOST("annee",'int');
    $sql ="select label, price, tva_tx, ref, tosell, tobuy, rowid, accountancy_code_sell, accountancy_code_buy ";
    $sql.=" FROM ".MAIN_DB_PREFIX."product where ref is not null and ref<>'' and fk_product_type=0 ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql)
    {
        $fnv=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/produitVente_".strval($annee)."_".date('Ymd').".csv";
        $outv=fopen($fnv,'w');
        $fna=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/produitAchat_".strval($annee)."_".date('Ymd').".csv";
        $outa=fopen($fna,'w');
        while ($objp = $db->fetch_object($resql))
        {
            if ($objp->tosell==1) 
            {
                $row=sprintf('"%s","%s",%.2f,"V-%s",%.4f', propre($objp->label), $objp->accountancy_code_sell, $objp->price, str_replace(' ','',$objp->ref), $objp->tva_tx);
                fwrite($outv,$row."\r\n");
            }
             
            if ($objp->tobuy==1) 
            {
                //trouver le dernier prix d'achat pour ce produit
                $sql ="select unitprice, tva_tx from ".MAIN_DB_PREFIX."product_fournisseur_price where fk_product=".$objp->rowid;
                $sql.=" order by tms desc limit 1";
                $re1sql = $db->query($sql);
                $obja = $db->fetch_object($re1sql);
                if ($obja)
                {
                    $row=sprintf('"%s","%s",%.2f,"A-%s",%.4f', propre($objp->label), $objp->accountancy_code_buy, $obja->unitprice, str_replace(' ','',$objp->ref), $obja->tva_tx);
                } else {
                    $row=sprintf('"%s","%s",%.2f,"A-%s",%.4f', propre($objp->label), $objp->accountancy_code_buy, 0, str_replace(' ','',$objp->ref), $objp->tva_tx);
                }
                fwrite($outa,$row."\r\n");
                $db->free($re1sql);
            }
        }
        fclose($outv);
        fclose($outa);
        $db->free($resql);
        updateNbreFichier($db,dirname($fnv));
        $msg=$langs->trans("PHPComptaMsgProduit", basename($fnv), basename($fna));
    } else {
        $msg=$langs->trans("PHPComptaMsgNoFile");
    }
    $sql="select count(*) as nbre from ".MAIN_DB_PREFIX."product where (ref is null or ref<>'') and fk_product_type=0 ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql) {
        $objp = $db->fetch_object($resql);
        if ($objp->nbre > 0) $erreur=$langs->trans("PHPComptaMsgNbre",strval($objp->nbre));
    }
}

if ($action=="ficheservice") {
    // champs de sorrtie Noalyss : nom service, poste comptable, prix vente, quick code, taux TVA
    $annee = GETPOST("annee",'int');
    $sql ="select label, price, tva_tx, ref, tosell, tobuy, rowid, accountancy_code_sell, accountancy_code_buy";
    $sql.=" FROM ".MAIN_DB_PREFIX."product where ref is not null and ref<>'' and fk_product_type=1 ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql)
    {
        $fnv=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/serviceVente_".strval($annee)."_".date('Ymd').".csv";
        $outv=fopen($fnv,'w');
        $fna=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/serviceAchat_".strval($annee)."_".date('Ymd').".csv";
        $outa=fopen($fna,'w');
        while ($objp = $db->fetch_object($resql))
        {
            if ($objp->tosell==1) 
            {
                $rowv=sprintf('"%s","%s",%.2f,"V-%s",%.4f', propre($objp->label), $objp->accountancy_code_sell, $objp->price, str_replace(' ','',$objp->ref), $objp->tva_tx);
                fwrite($outv,$row."\r\n");
            }
            if ($objp->tobuy==1) 
            {
                //trouver le dernier prix d'achat pour ce service
                $sql ="select unitprice, tva_tx from ".MAIN_DB_PREFIX."product_fournisseur_price where fk_product=".$objp->rowid;
                $sql.=" order by tms desc limit 1";
                $re1sql = $db->query($sql);
                $obja = $db->fetch_object($re1sql);
                if ($obja)
                {
                    $row=sprintf('"%s","%s",%.2f,"A-%s",%.4f', propre($objp->label), $objp->accountancy_code_buy, $obja->unitprice, str_replace(' ','',$objp->ref), $obja->tva_tx);
                } else {
                    $row=sprintf('"%s","%s",%.2f,"A-%s",%.4f', propre($objp->label), $objp->accountancy_code_buy, 0, str_replace(' ','',$objp->ref), $objp->tva_tx);
                }
                fwrite($outa,$row."\r\n");
                $db->free($re1sql);
            }            
        }
        fclose($outv);
        fclose($outa);
        $db->free($resql);
        updateNbreFichier($db,dirname($fnv));
        $msg=$langs->trans("PHPComptaMsgService", basename($fnv), basename($fna));
    } else {
        $msg=$langs->trans("PHPComptaMsgNoFile");
    }
    $sql="select count(*) as nbre from ".MAIN_DB_PREFIX."product where (ref is null or ref<>'') and fk_product_type=1 ";
    if ($annee>0) {
        $sql.=" and DATE_FORMAT(tms,'%Y')=".strval($annee);
    }
    $resql = $db->query($sql);
    if ($resql) {
        $objp = $db->fetch_object($resql);
        if ($objp->nbre > 0) $erreur=$langs->trans("PHPComptaMsgNbre",strval($objp->nbre));
    }
}

if ($action=="facturevente") {
    $dted = GETPOST("dted",'alpha');
    $dtef = GETPOST("dtef",'alpha');
    $dte=explode("/",$dted);
    $dtedUS=sprintf("%04d-%02d-%02d",$dte[2],$dte[1],$dte[0]);
    $dd=mktime(0,0,0,$dte[1],$dte[0],$dte[2]);
    $dte=explode("/",$dtef);
    $dtefUS=sprintf("%04d-%02d-%02d",$dte[2],$dte[1],$dte[0]);
    $df=mktime(0,0,0,$dte[1],$dte[0],$dte[2]);
    $nop=1;
    $sql ="select f.rowid, facnumber, code_client, datef, paye, amount, tva, total, total_ttc, f.note_public, type, fk_soc, c.nom, f.ref ";
    $sql.=" from ".MAIN_DB_PREFIX."facture f, ".MAIN_DB_PREFIX."societe c where c.rowid=f.fk_soc and type=0 "; //and c.code_client is not null and c.code_client<>'' ";
    $sql.=" and f.datef>='".$dtedUS."' and f.datef<='".$dtefUS."' order by facnumber";
    $resql = $db->query($sql);
    if ($resql)
    {
        $fn=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/factureVente_".$dtedUS."_".$dtefUS.".csv";
        $out=fopen($fn,'w');
        while ($objp = $db->fetch_object($resql))
        {
            if (!$objp->code_client) {
                $erreurfact.="<li>".$langs->trans("PHPComptaMsgFactClient",$objp->ref)."</li>";
            } else {
                $tmp=explode("-",$objp->datef);
                $dte=sprintf('%2d.%2d.%4d', $tmp[2], $tmp[1], $tmp[0]);
                $dte=str_replace(' ','0',$dte);
                $libelle=propre($langs->trans("PHPComptaVenteLabel",$objp->nom));
                $entete=sprintf('%d;"%s";"%s";"%s";"%s";"%s";;;;;', $nop, $dte, 'T', $objp->code_client, $libelle, $objp->ref);

                $sql ="select f.rowid, f.description, f.tva_tx, f.qty, f.price, f.total_ht, f.total_tva, f.total_ttc, p.ref ";
                $sql.=" from ".MAIN_DB_PREFIX."facturedet f, ".MAIN_DB_PREFIX."product p ";
                $sql.=" where p.rowid=f.fk_product and fk_facture=".$objp->rowid;
                $detailsql = $db->query($sql);
                $rec=array();
                while ($detail = $db->fetch_object($detailsql))
                {
                    if (empty($detail->ref)) {
                        $erreurfact.="<li>".$langs->trans("PHPComptaMsgFactDetail", propre($detail->description), $objp->ref)."</li>";
                    } else {
                        $row=sprintf('%d;"%s";"%s";"V-%s";"%s";"%s";%2.f;%.2f;%d;%.2f;%.2f', $nop, $dte, 'S', str_replace(' ','',$detail->ref), propre($detail->description), '', $detail->price ,$detail->total_tva, $detail->qty, intval($detail->tva_tx)/100, $detail->total_ttc);
                        array_push($rec,$row);
                    }
                }
                if (count($rec)>0) {
                    fwrite($out,$entete."\r\n");
                    foreach ($rec as $row) fwrite($out,$row."\r\n");
                } else {
                    $erreurfact.="<li>".$langs->trans("PHPComptaMsgFactVide", $objp->ref)."</li>";
                }
                $db->free($detailsql);
                $nop++;
            }
        }
        fclose($out);
        $db->free($resql);
        updateNbreFichier($db,dirname($fn));
        $msg=$langs->trans("PHPComptaMsgVente", basename($fn));
    } else {
        $msg=$langs->trans("PHPComptaMsgNoFile");
    }
}

if ($action=="factureachat") {
    $dteda = GETPOST("dteda",'alpha');
    $dtefa = GETPOST("dtefa",'alpha');
    $dte=explode("/",$dteda);
    $dtedUS=sprintf("%04d-%02d-%02d",$dte[2],$dte[1],$dte[0]);
    $dd=mktime(0,0,0,$dte[1],$dte[0],$dte[2]);
    $dte=explode("/",$dtefa);
    $dtefUS=sprintf("%04d-%02d-%02d",$dte[2],$dte[1],$dte[0]);
    $df=mktime(0,0,0,$dte[1],$dte[0],$dte[2]);
    $nop=1;
    $sql ="select f.rowid, f.ref, code_fournisseur, datef, paye, amount, tva, total_ht, total_ttc, f.note_public, type, fk_soc, c.nom ";
    $sql.=" from ".MAIN_DB_PREFIX."facture_fourn f, ".MAIN_DB_PREFIX."societe c where c.rowid=f.fk_soc and type=0 and c.code_fournisseur is not null and c.code_fournisseur<>'' ";
    $sql.=" and f.datef>='".$dtedUS."' and f.datef<='".$dtefUS."' order by ref";
    $resql = $db->query($sql);
    if ($resql)
    {
        $fn=$dolibarr_main_data_root."/ecm/".$conf->global->NOALYSS_DIR."/factureAchat_".$dtedUS."_".$dtefUS.".csv";
        $out=fopen($fn,'w');
        while ($objp = $db->fetch_object($resql))
        {
            if (!$objp->code_fournisseur) {
                $erreurfact.="<li>".$langs->trans("PHPComptaMsgFactFour",$objp->ref)."</li>";
            } else {
                $tmp=explode("-",$objp->datef);
                $dte=sprintf('%2d.%2d.%4d', $tmp[2], $tmp[1], $tmp[0]);
                $dte=str_replace(' ','0',$dte);
                $libelle=propre($langs->trans("PHPComptaAchatLabel",$objp->nom));
                $entete=sprintf('%d;"%s";"%s";"%s";"%s";"%s";;;;;', $nop, $dte, 'T', $objp->code_fournisseur, $libelle, $objp->ref);

                $sql ="select f.rowid, f.description, f.tva_tx, f.qty, f.pu_ht, f.total_ht, f.tva, f.total_ttc, p.ref ";
                $sql.=" from ".MAIN_DB_PREFIX."facture_fourn_det f, ".MAIN_DB_PREFIX."product p ";
                $sql.=" where p.rowid=f.fk_product and fk_facture_fourn=".$objp->rowid;
                $detailsql = $db->query($sql);
                $rec=array();
                while ($detail = $db->fetch_object($detailsql))
                {
                    if (empty($detail->ref)) {
                        $erreurfact.="<li>".$langs->trans("PHPComptaMsgFactDetail", propre($detail->description), $objp->ref)."</li>";
                    } else {
                        $row=sprintf('%d;"%s";"%s";"A-%s";"%s";"%s";%2.f;%.2f;%d;%.2f;%.2f', $nop, $dte, 'S', str_replace(' ','',$detail->ref), propre($detail->description), '', $detail->pu_ht ,$detail->total_tva, $detail->qty, intval($detail->tva_tx)/100, $detail->total_ttc);
                        array_push($rec,$row);
                    }
                }
                if (count($rec)>0) {
                    fwrite($out,$entete."\r\n");
                    foreach ($rec as $row) fwrite($out,$row."\r\n");
                } else {
                    $erreurfact.="<li>".$langs->trans("PHPComptaMsgFactVide", $objp->ref)."</li>";
                }
                $db->free($detailsql);
                $nop++;
            }
        }
        fclose($out);
        $db->free($resql);
        updateNbreFichier($db,dirname($fn));
        $msg=$langs->trans("PHPComptaMsgAchat", basename($fn));
    } else {
        $msg=$langs->trans("PHPComptaMsgNoFile");
    }
}
/*
 * View
 */

$html=new Form($db);

llxHeader('',$langs->trans("PHPComptaArea"));

print_fiche_titre($langs->trans("PHPComptaArea"));

print $langs->trans("PHPComptaDesc1").'<br/>';
print $langs->trans("PHPComptaDesc2").'<br/> ';
print $langs->trans("PHPComptaDesc3").'<br/>';
print $langs->trans("PHPComptaDesc4").'<br/>';
print $langs->trans("PHPComptaDirDest",$conf->global->NOALYSS_DIR).'<br/>';
print '<br/>';
 /*
  * fiches client
  */
print '<form name="ficheclient" action="'.$_SERVER["PHP_SELF"].'" method="POST">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="ficheclient">';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("PHPComptaTypeFicheC").'</td>';
print '<td>'.$langs->trans("PHPComptaParam").'</td>';
print '<td>&nbsp;</td>';
print '</tr>';
$val=true;
print '<tr '.$bc[$val].'>';
print "<td width='60%'>".$langs->trans("PHPComptaFicheClient").'</td>';
print "<td><select name='annee' value=''><option value='0'>".$langs->trans("PHPComptaAll")."</option>";
$sql = "SELECT distinct DATE_FORMAT(tms,'%Y') as annee ";
$sql.= " FROM ".MAIN_DB_PREFIX."societe where client=1 order by 1 desc";
$resql = $db->query($sql);
if ($resql)
{
    $num = $db->num_rows($resql);
    $i = 0;
    while ($i < $num)
    {
        $objp = $db->fetch_object($resql);
        print "<option value=".$objp->annee.">".$objp->annee."</option>";
        $i++;
    }
    $db->free($resql);
}
print "</select></td>";
print "<td width='10%'><input type=\"submit\" name=\"save\" class=\"button\" value=\"".$langs->trans("PHPCOmptaGenerer")."\">";
print "</td></tr></table>";
print "</form>\n";

print "<br/><br/>";

 /*
  * fiches fournisseurs
  */
print '<form name="fichefournisseur" action="'.$_SERVER["PHP_SELF"].'" method="POST">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="fichefournisseur">';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("PHPComptaTypeFicheF").'</td>';
print '<td>'.$langs->trans("PHPComptaParam").'</td>';
print '<td>&nbsp;</td>';
print '</tr>';
$val=true;
print '<tr '.$bc[$val].'>';
print "<td width='60%'>".$langs->trans("PHPComptaFicheFournisseur").'</td>';
print "<td><select name='annee' value=''><option value='0'>".$langs->trans("PHPComptaAll")."</option>";
$sql = "SELECT distinct DATE_FORMAT(tms,'%Y') as annee ";
$sql.= " FROM ".MAIN_DB_PREFIX."societe where fournisseur=1 order by 1 desc";
$resql = $db->query($sql);
if ($resql)
{
    $num = $db->num_rows($resql);
    $i = 0;
    while ($i < $num)
    {
        $objp = $db->fetch_object($resql);
        print "<option value=".$objp->annee.">".$objp->annee."</option>";
        $i++;
    }
    $db->free($resql);
}
print "</select></td>";
print "<td width='10%'><input type=\"submit\" name=\"save\" class=\"button\" value=\"".$langs->trans("PHPCOmptaGenerer")."\">";
print "</td></tr></table>";
print "</form>\n";

print "<br/><br/>";

/*
* fiches produits
*/

print '<form name="ficheproduit" action="'.$_SERVER["PHP_SELF"].'" method="POST">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="ficheproduit">';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("PHPComptaTypeFicheP").'</td>';
print '<td>'.$langs->trans("PHPComptaParam").'</td>';
print '<td>&nbsp;</td>';
print '</tr>';
$val=true;
print '<tr '.$bc[$val].'>';
print "<td width='60%'>".$langs->trans("PHPComptaFicheProduit").'<br/>'.$langs->trans("PHPComptaFicheProduitsRem").'</td>';
print "<td><select name='annee' value=''><option value='0'>".$langs->trans("PHPComptaAll")."</option>";
$sql = "SELECT distinct DATE_FORMAT(tms,'%Y') as annee ";
$sql.= " FROM ".MAIN_DB_PREFIX."product where fk_product_type=0 order by 1 desc";
$resql = $db->query($sql);
if ($resql)
{
    $num = $db->num_rows($resql);
    $i = 0;
    while ($i < $num)
    {
        $objp = $db->fetch_object($resql);
        print "<option value=".$objp->annee.">".$objp->annee."</option>";
        $i++;
    }
    $db->free($resql);
}
print "</select></td>";
print "<td width='10%'><input type=\"submit\" name=\"save\" class=\"button\" value=\"".$langs->trans("PHPCOmptaGenerers")."\">";
print "</td></tr></table>";
print "</form>\n";

print "<br/><br/>";

 /*
  * fiches service
  */
print '<form name="ficheservice" action="'.$_SERVER["PHP_SELF"].'" method="POST">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="ficheservice">';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("PHPComptaTypeFicheS").'</td>';
print '<td>'.$langs->trans("PHPComptaParam").'</td>';
print '<td>&nbsp;</td>';
print '</tr>';
$val=true;
print '<tr '.$bc[$val].'>';
print "<td width='60%'>".$langs->trans("PHPComptaFicheService").'<br/>'.$langs->trans("PHPComptaFicheServiceRem").'</td>';
print "<td><select name='annee' value=''><option value='0'>".$langs->trans("PHPComptaAll")."</option>";
$sql = "SELECT distinct DATE_FORMAT(tms,'%Y') as annee ";
$sql.= " FROM ".MAIN_DB_PREFIX."product where fk_product_type=1 order by 1 desc";
$resql = $db->query($sql);
if ($resql)
{
    $num = $db->num_rows($resql);
    $i = 0;
    while ($i < $num)
    {
        $objp = $db->fetch_object($resql);
        print "<option value=".$objp->annee.">".$objp->annee."</option>";
        $i++;
    }
    $db->free($resql);
}
print "</select></td>";
print "<td width='10%'><input type=\"submit\" name=\"save\" class=\"button\" value=\"".$langs->trans("PHPCOmptaGenerers")."\">";
print "</td></tr></table>";
print "</form>\n";

print "<br/><br/>";

 /*
  * Les factures de ventes
  */

print '<form name="facturevente" action="'.$_SERVER["PHP_SELF"].'" method="POST">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="facturevente">';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("PHPComptaTypeFicheV").'</td>';
print '<td>'.$langs->trans("PHPComptaParam").'</td>';
print '<td>&nbsp;</td>';
print '</tr>';
$val=true;
print '<tr '.$bc[$val].'>';
print "<td width='60%'>".$langs->trans("PHPComptaFactureVenteD")."</td>";
print "<td>";
print $html->select_date($dd,'dted','','',0,'editdate');
print "</td>";
print "<td width='10%'>&nbsp;</td>";
print '</tr><tr '.$bc[!$val].'>';
print "<td width='40%'>".$langs->trans("PHPComptaFactureVenteF").'</td>';
print "<td>";
print $html->select_date($df,'dtef','','',0,'editdate');
print "</td>";
print "<td width='10%'><input type=\"submit\" name=\"save\" class=\"button\" value=\"".$langs->trans("PHPCOmptaGenerer")."\">";
print "</td></tr></table>";
print "</form>\n";

print "<br/><br/>";

 /*
  * Les factures d'achats
  */

print '<form name="factureachat" action="'.$_SERVER["PHP_SELF"].'" method="POST">';
print '<input type="hidden" name="token" value="'.$_SESSION['newtoken'].'">';
print '<input type="hidden" name="action" value="factureachat">';
print '<table class="noborder" width="100%">';
print '<tr class="liste_titre">';
print '<td>'.$langs->trans("PHPComptaTypeFicheA").'</td>';
print '<td>'.$langs->trans("PHPComptaParam").'</td>';
print '<td>&nbsp;</td>';
print '</tr>';
$val=true;
print '<tr '.$bc[$val].'>';
print "<td width='60%'>".$langs->trans("PHPComptaFactureAchatD")."</td>";
print "<td>";
print $html->select_date($dd,'dteda','','',0,'editdate');
print "</td>";
print "<td width='10%'>&nbsp;</td>";
print '</tr><tr '.$bc[!$val].'>';
print "<td width='40%'>".$langs->trans("PHPComptaFactureAchatF").'</td>';
print "<td>";
print $html->select_date($df,'dtefa','','',0,'editdate');
print "</td>";
print "<td width='10%'><input type=\"submit\" name=\"save\" class=\"button\" value=\"".$langs->trans("PHPCOmptaGenerer")."\">";
print "</td></tr></table>";
print "</form>\n";


if ($msg) {
    print "<br/><p align='center'>".$msg."</p>";
}
if ($erreur) {
    print "<p align='center' style='color: red;'>".$erreur."</p>";
}

if ($erreurfact) {
    print "<p style='color: red;'><ul>".$erreurfact."</ul></p>";
}

$db->close();
llxFooter('$Date: 2020/01/18 14:22:23 $ - $Revision: 0.5 $');
?>
